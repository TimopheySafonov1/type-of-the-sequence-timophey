﻿// See https://aka.ms/new-console-template for more information
using ClassLibrary1;
using System.Runtime.InteropServices;
Console.WriteLine("Enter the number");
string? input = Console.ReadLine();
long source;
long.TryParse(input, out source);
string? result = TypeOfSequence.Name(source);
Console.WriteLine($"{result}");
Console.ReadKey(true);
